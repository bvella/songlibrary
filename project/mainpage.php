<?php
    session_start();
?>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-9  col-md-10" style="background-color:powderblue;" >
                    <form method="post" action="mainpage.php" class="text-center">
                        <p><input type="submit" name="submit" value="Search"><input type="text" name="srch"></p>
                    </form>
                    <?php
                        //button display depending on authority level
                        if($_SESSION['access'] == '2'){
                    ?>  
                            <input type="button" value="Add song" onclick="window.location.href='add.php'">
                            <input type="button" value="Remove song" onclick="window.location.href='remove.php'">
                    <?php 
                    }
                    ?>
                    <input type="button" value="User info" onclick="window.location.href='user.php'">
                    <input type="button" value="Log out" onclick="window.location.href='index.php'">
                        
                    <div>
                        <?php //display all list of tbl_songs records
                        require_once("connect.php");
                        $query = "SELECT * FROM tbl_songs";
                        $result = mysqli_query($conn, $query)
                                or die("Error in query: ". mysqli_error($conn));
                        $row = mysqli_fetch_row($result);
                        $count = $row[0];
                        if($count > 0){
                            //output in table format
                            $str = "<div class='text-center'><table><tr><th>id</th><th>title</th><th>date released</th><th>rating</th></tr>";
                            while ($row = $result->fetch_assoc()){
                                $str .= "<tr><td>".$row["id"]."</td><td>".$row["name"]."</td> <td>".$row["date_released"]."</td><td>".$row["rating"]."</td><td><img src='images/".$row["images"]."' height='42' width='42'></td><td><audio><source src='audio/".$row["audiofile"]."' type='audio/mpeg'></audio></td></tr>";
                            }
                                echo $str."</table></div>";
                        }
                        else {
                            echo "0 results";
                        }
                        mysqli_free_result($result);
                        mysqli_close($conn);
                        ?>
                    </div>
                    <div class="row" class='text-center'>
                            <input type="button" value="sort" onclick="window.location.href='mainpage.php?action=sort&field=name'">
                        </div>

                </div>
                <div class="col-sm-3  col-md-2" style="background-color:coral;">
                    <?php //playlist
                        $srch = mysqli_real_escape_string($conn,$_POST['srch']);
                        $query = "SELECT * FROM tbl_playlist_songs WHERE playlist_id ='".$_SESSION['id']."'";//obtains playlist correponding to current user
                        $result = mysqli_query($conn, $query)
                                or die("Error in query: ". mysqli_error($conn));
                    if($result -> num_rows > 0){
                        while ($row = $result->fetch_assoc()){//fetches data from database
                                $query = "SELECT * FROM tbl_songs WHERE song_id =".$row['song_id'];
                                $result = mysqli_query($conn, $query)
                                    or die("Error in query: ". mysqli_error($conn));
                                $song = $result->fetch_assoc();
                                echo $song["name"]."<a href='audio/".$song["name"]."'></a></br>";
                                }
                    } else {
                            echo "0 results";
                        }
            
                    ?>
                            </div>

                        </div>

                    </div>
                    <?php //sorting button
                        $srch = mysqli_real_escape_string($conn,$_POST['srch']);
                        $query = "SELECT * FROM tbl_songs"; 
                        if ($srch != ""){
                            $query .= "WHERE name LIKE '$srch'";
                        }
                        if (isset($_GET['action']) && $_GET['action'] == "sort") {
                            $query .= " ORDER BY ".mysqli_real_escape_string($conn, $_GET['field']);
                        }

                        $result = mysqli_query($conn, $query)
                                or die("Error in query: ". mysqli_error($conn));
                        if($result -> num_rows > 0){
                            $str = "<table><tr><th>id</th><th>title</th><th>date released</th><th>rating</th><th>cover picture</th> <th>audiofile</th></tr>";
                            while ($row = $result->fetch_assoc()){
                                $str .= "<tr><td>".$row["id"]."</td> <td>".$row["name"]."</td> <td>".$row["date_released"]."</td><td>".$row["rating"]."</td><td><img src='images/".$row["images"]."' height='42' width='42'></td><td><audio><source src='audio/".$row["audiofile"]."' type='audio/mpeg'></audio></td></tr>";
                                $query = "SELECT * FROM tbl_books";
                
                            }
                                echo $str."<table>";
                        }
                            else {
                                echo "0 results";
                        }
                    mysqli_free_result($result);
                    mysqli_close($conn);


                    ?>
    
    </body>
</html>