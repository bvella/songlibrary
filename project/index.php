<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        
    </head>
    <body>
        <h1>Login</h1>
        <form method="post" action="index.php">
            <p>Enter your username:</p>
            <input type="text" name ="uname">
            <p>Enter your password:</p>
            <input type="password" name ="password">
            <br>
            <input type="submit" value="submit" name= "Log In">
            <input type="button" value="Register Now" onclick="window.location.href='registration.php'">  
        </form>
        <?php
            //check for empty inputs
            if((empty($_POST['uname'])) || (empty($_POST['password']))){
                echo "<br>All values must be set";
            }else{
                require_once("connect.php");
                $uname = mysqli_real_escape_string($conn,$_POST['uname']);
                $password = mysqli_real_escape_string($conn,$_POST['password']);
                //checks if username is in database
                $query = "SELECT * FROM tbl_users WHERE username = '$uname'";//searches users table in database for the username inputted
                $result = mysqli_query($conn, $query)
                    or die("Error in query: ". mysqli_error($conn));
                $row = mysqli_fetch_assoc($result);
                if(password_verify ($password , $row['password'] )){//once user is found, password is validated
                    $_SESSION['id'] = $row['id'];//session id is used for the user's page
                    $_SESSION['access'] = $row['authority_level'];
                    if($row['authority_level'] == '3'){//if administrator load admin.php, users and managers load mainpage.php
                        header("Location: http://localhost/project/admin.php");
                    }
                    else{
                        header("Location: http://localhost/project/mainpage.php");
                    }
                    
                }
                else{//if username not found, output message
                    echo "Username and password do not match. Have you registered?";  
                        }
                mysqli_free_result($result);
                    mysqli_close($conn);

            }
        

    ?>
    </body>
</html>