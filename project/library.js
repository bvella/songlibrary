function checkPassword(check){
    //first check for length then for special characters
    var re = RegExp("^(?=.*[a-z])(?=.*[!@#\$%\^&\*])");
    var pwcheck = check.value;
    if(pwcheck.length < 6){
        alert("Your password needs to be made up of at least 6 characters!");
    }
    else if(re.exec(pwcheck)!=0){
        alert("Your password needs to be made up of at least one special character or digit!");
    }
}
function checkdigits(check){
    //used for validation of names and surnames
    var re = RegExp("^(?=.*[0-9])|(?=.*[!@#\$%\^&\*])");
    var nodigit = check.value;
    if(re.exec(nodigit)>0){
        alert("Invalid input. Digits are not allowed.");
    }
}
function checkemail(check){
    //validation of email
    var i = 0,x = 0;
    do{
        if(check.charAt(i)=='@'){
            x++;
        }
        i++
    }while(i<check.length())
        if(x == 0){
            alert("Invalid email. It must contain '@'");
        }
        else if(check.substring(check.length-4,check.length)!=".com"){
            alert("Invalid email!!");
        }
}


window.onload= function(){
    var uname = document.getElementById("uname");
    var pw = document.getElementById("password");
    var pwc = document.getElementById("pword");
    var name = document.getElementById("name");
    var sname = document.getElementById("sname");
    var email = document.getElementById("email");

    uname.value.required;
    pw.addEventListener('blur',function(){checkPassword(pw)},false);
    pwc.addEventListener('blur',function(){checkPassword(pwc)},false);
    name.addEventListener('blur',function(){checkdigits(name)},false);
    sname.addEventListener('blur',function(){checkdigits(sname)},false);
    email.addEventListener('blur',function(){checkemail(email)},false);
}
