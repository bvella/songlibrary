<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container-fluid">
                <div>
                    <h1>User information</h1>
                    <input type="button" value="Log out" onclick="window.location.href='index.php'">
                    <input type="button" value="Back" onclick="window.location.href='mainpage.php'">
                </div>
                <div class="row">
                <div class="col-sm-7  col-md-10" style="background-color:powderblue;">
                <?php   
                    require_once("connect.php");
                    //get session's user information from database and outputs it
                    $query = "SELECT * FROM tbl_users WHERE id ='".$_SESSION['id']."'";
                    $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                    $user = mysqli_fetch_assoc($result);
                    echo "<p>Username: ".$user['username']."</p>";
                    echo "<p>Name: ".$user['name']."</p>";
                    echo "<p>Surname: ".$user['surname']."</p>";
                    echo "<p>Email: ".$user['email']."</p>";
                    ?>
                </div>
                <div class="col-sm-5   col-md-2" style="background-color:coral;">
                    <?php             
                    //locates user's playlist songs and outputs them
                    $query = "SELECT * FROM tbl_playlist_songs WHERE playlist_id = '1'";
                    $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                    if($result -> num_rows > 0){
                        while ($row = $result->fetch_assoc()){
                             $query = "SELECT * FROM tbl_songs WHERE id =".$row['song_id'];
                             $result = mysqli_query($conn, $query)
                             or die("Error in query: ". mysqli_error($conn));
                                 $song = $result->fetch_assoc();
                                 echo "<a href='audio/".$song["name"]."'> ".$song["name"]."</a></br>";
                                }
                                }
                                ?>
                  </div>  
            </div>
        </div>
        <input type="button" value="Back to Main page" onclick="window.location.href='mainpage.php'">
    </body>
</html>
                        