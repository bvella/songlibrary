<!DOCTYPE html>
<html>
    <head>
        <!-- external js file -->
        <script src="library.js"></script>
    </head>
    <body>
        <h1>Registration form</h1>       
        <form method="post" action="registration.php">
            <p>Username: <input type="text" name="uname" id="uname"></p>
            <p>Password:(at least 6 characters with one special character)</p> 
            <input type="password" name="password" id="password">
            <p>Confirm password: <input type="password" name="pword" id="pword"></p>
            <p>Name: <input type="text" name="name" id="name"></p>
            <p>Surname: <input type="text" name="sname" id="sname"></p>
            <p>Email: <input type="email" name="email" id="email"></p>
            <p><input type="submit" name="submit" value="Register"></p>   
        </form>
        <?php
        //check for empty values
        if (isset( $_POST['submit'] ) ){
            if((empty($_POST['uname'])) || (empty($_POST['password'])) || (empty($_POST['pword'])) || (empty($_POST['name'])) || (empty($_POST['sname'])) || (empty($_POST['email']))){
                echo "<br>All values must be set";
            }else{
                require_once("connect.php");
                //stores the values in variables and avoids sql injection
                $uname = mysqli_real_escape_string($conn,$_POST['uname']);
                $password = mysqli_real_escape_string($conn,$_POST['password']);
                $pword = mysqli_real_escape_string($conn,$_POST['pword']);
                $name = mysqli_real_escape_string($conn,$_POST['name']);
                $sname = mysqli_real_escape_string($conn,$_POST['sname']);
                $email = mysqli_real_escape_string($conn,$_POST['email']);
                if($password != $pword){
                    echo "Password confirmation does not match password.";
                } else{
                    //check that username is unique
                    $query = "SELECT COUNT(*) FROM tbl_users WHERE username = '$uname'";
                    $result = mysqli_query($conn, $query)
                        or die("Error in query: ". mysqli_error($conn));
                    $row = mysqli_fetch_row($result);
                    $count = $row[0];
                    if($count <= 0){
                        //hashes password for security reasons
                        $hashed_password = password_hash($password,PASSWORD_DEFAULT);

                        $query = "INSERT INTO tbl_users (username,password,name,surname,email)
                        VALUES ('$uname', '$hashed_password','$name','$sname','$email')";
                        mysqli_query($conn, $query)
                        or die("Error in query: ". mysqli_error($conn));
                        echo("You have been registered");
                        //on valid registration loads back index.php
                        header("Location: http://localhost/project/index.php");
                        exit();

                    } else {
                        //if username already exists output:
                        echo "already exists";
                    }

                    mysqli_free_result($result);
                    mysqli_close($conn);
                }
            }
        }
        ?>
    </body>
</html>